package com.example.demo.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Album;
import com.example.demo.Model.Artist;

@Service
public class ArtistService {

    @Autowired
    AlbumService albumService;

    Artist DanTruongVol1 = new Artist(10,"Đan Trường");

    Artist PhuongThanhvol1 = new Artist(11,"Phương Thanh");

    public ArrayList<Artist> getArtistList() {
        ArrayList<Artist> artistList = new ArrayList<>();
        
        DanTruongVol1.setAlbums(albumService.getAlbumDanTruong());
        PhuongThanhvol1.setAlbums(albumService.getAlbumPhuongThanh());

        artistList.add(DanTruongVol1);
        artistList.add(PhuongThanhvol1);
    
        return artistList;
    }
}
