package com.example.demo.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Artist;
import com.example.demo.Service.ArtistService;

@RestController
public class ArtistController {
    @Autowired
    private ArtistService artistService;

    @GetMapping("/artist-info")
    public Artist getCountryInfo(@RequestParam("artistId") int artistId){
        ArrayList<Artist> artists = artistService.getArtistList();

        Artist findArtist = new Artist();
        for(Artist artist : artists){
            if(artist.getId() == artistId){
                findArtist = artist ;
            }
        }
        return findArtist;
    }
   
    @GetMapping("/artists")
    public ArrayList<Artist> getCountry(){
        ArrayList<Artist> allArtist = artistService.getArtistList();
        return allArtist;
    }
}
